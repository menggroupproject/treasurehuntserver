package hw.treasureHunt.service.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "metric")
public class Metric implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1282149782089584692L;
	
	private Long distance;
    private Long steps;
    private String userId;
    private Date start;
    private Date end;
    
    
    private Map<Integer,PeriodicData> data;
	
	
	public Metric(){
		super();
	}
	

	
	public Metric(Long distance, Long steps, String userId, Date start, Date end) {
		super();
		this.distance = distance;
		this.steps = steps;
		this.userId = userId;
		this.start = start;
		this.end = end;
	}



	public Long getSteps() {
		return steps;
	}

	public void setSteps(Long steps) {
		this.steps = steps;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getDistance() {
		return distance;
	}

	public void setDistance(Long distance) {
		this.distance = distance;
	}



	public Map<Integer,PeriodicData> getData() {
		return data;
	}



	public void setData(Map<Integer,PeriodicData> data) {
		this.data = data;
	}
	


}
