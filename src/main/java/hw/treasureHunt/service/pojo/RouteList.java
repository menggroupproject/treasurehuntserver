package hw.treasureHunt.service.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlRootElement (name="routes")
public class RouteList implements Serializable 
{
    private static final long serialVersionUID = 1L;
     
    private List<Route> routes = new ArrayList<Route>();
 
    public List<Route> getRoutes() {
        return routes;
    }
 
    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}