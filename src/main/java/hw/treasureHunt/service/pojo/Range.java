package hw.treasureHunt.service.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "range")
public class Range implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5309043104024185541L;
	@XmlElement
	private Double mRange;
	@XmlElement
	private Double sRange;
	public Range(Double mRange, Double sRange) {
		super();
		this.mRange = mRange;
		this.sRange = sRange;
	}
	public Range() {
		super();
	}
	public Double getmRange() {
		return mRange;
	}
	public void setmRange(Double mRange) {
		this.mRange = mRange;
	}
	public Double getsRange() {
		return sRange;
	}
	public void setsRange(Double sRange) {
		this.sRange = sRange;
	}
	
	

	
}
