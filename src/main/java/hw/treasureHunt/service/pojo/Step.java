package hw.treasureHunt.service.pojo;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement (name = "step")
public class Step implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1568792736555989336L;
	
	@XmlElement
	private String stepNo;
	@XmlElement
	private String task;
	@XmlElement
	private String solution;
	@XmlElement
	private String name;
	@XmlElement
	private List<Question> questions;
	@XmlElement
	private String treasure;
	@XmlElement
	private List<Point> points;
	
	
	
	public Step() {
		super();
	}
	public Step(String stepNo, String task, String solution, String name, List<Question> questions, String treasure,
			List<Point> points) {
		super();
		this.stepNo = stepNo;
		this.task = task;
		this.solution = solution;
		this.name = name;
		this.questions = questions;
		this.treasure = treasure;
		this.points = points;
	}
	public String getStepNo() {
		return stepNo;
	}
	public void setStepNo(String stepNo) {
		this.stepNo = stepNo;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public String getSolution() {
		return solution;
	}
	public void setSolution(String solution) {
		this.solution = solution;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Question> getQuestions() {
		return questions;
	}
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}
	public String getTreasure() {
		return treasure;
	}
	public void setTreasure(String treasure) {
		this.treasure = treasure;
	}
	public List<Point> getPoints() {
		return points;
	}
	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	
	

}
