package hw.treasureHunt.service.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "point")
public class Point implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6628714918904280802L;
	@XmlElement
	private Double latitude;
	@XmlElement
	private String name;
	@XmlElement
	private Range hr;
	@XmlElement
	private Integer pointNo;
	@XmlElement
	private Double longitude;
	public Point(Double latitude, String name, Range hr, Integer pointNo, Double longitude) {
		super();
		this.latitude = latitude;
		this.name = name;
		this.hr = hr;
		this.pointNo = pointNo;
		this.longitude = longitude;
	}
	public Point() {
		super();
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Range getHr() {
		return hr;
	}
	public void setHr(Range hr) {
		this.hr = hr;
	}
	public Integer getPointNo() {
		return pointNo;
	}
	public void setPointNo(Integer pointNo) {
		this.pointNo = pointNo;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	
	

}
