package hw.treasureHunt.service.pojo;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
@Document(collection = "images")
public class Image implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4865029391126100957L;

	//@Id
	private String _id;

	
	private String encodedImage;

	
	
	public Image(String encodedImage) {
		super();
		this.encodedImage = encodedImage;

	}




	public Image() {
		super();
	}




	public String getEncodedImage() {
		return encodedImage;
	}


	public void setEncodedImage(String encodedImage) {
		this.encodedImage = encodedImage;
	}


	public String get_id() {
		return _id;
	}


	public void set_id(String _id) {
		this._id = _id;
	}
	
	
}
