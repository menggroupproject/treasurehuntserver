package hw.treasureHunt.service.pojo;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name="question")
public class Question implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3105135200393299945L;
	@XmlElement
	private String question;
	@XmlElement
	private String isImage;
	@XmlElement
	private String correctAnswer;
	@XmlElement
	private List<String> answers;
	@XmlElement
	private String image;
	
	
	public Question() {
		super();
	}


	public Question(String question, String isImage, String correctAnswer, List<String> answers) {
		super();
		this.question = question;
		this.isImage = isImage;
		this.correctAnswer = correctAnswer;
		this.answers = answers;
	}
	

	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getQuestion() {
		return question;
	}


	public void setQuestion(String question) {
		this.question = question;
	}


	public String getIsImage() {
		return isImage;
	}


	public void setIsImage(String isImage) {
		this.isImage = isImage;
	}


	public String getCorrectAnswer() {
		return correctAnswer;
	}


	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}


	public List<String> getAnswers() {
		return answers;
	}


	public void setAnswers(List<String> answers) {
		this.answers = answers;
	}
	
	

	
	
}
