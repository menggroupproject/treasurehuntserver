package hw.treasureHunt.service.pojo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "routes")
@XmlRootElement (name = "route")
@XmlAccessorType(XmlAccessType.NONE)
public class Route implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2153656245908435863L;
	
	@XmlElement
	private String name;
	@XmlElement (name = "steps")
	private List<Step> steps;
	
	
	public Route() {
		super();
	}
	public Route(String name, List<Step> steps) {
		super();
		this.name = name;
		this.steps = steps;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Step> getSteps() {
		return steps;
	}
	public void setSteps(List<Step> steps) {
		this.steps = steps;
	}
	
	

}
