package hw.treasureHunt.service;


import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import hw.treasureHunt.repository.ImageRepository;
import hw.treasureHunt.repository.RouteRepository;
import hw.treasureHunt.repository.StatRepository;
import hw.treasureHunt.service.pojo.Image;
import hw.treasureHunt.service.pojo.Metric;
import hw.treasureHunt.service.pojo.Route;
import hw.treasureHunt.service.pojo.RouteList;

@RestController
public class DataRestApi {

	@Autowired
	private StatRepository metricRepository;
	
	
	@Autowired
	private RouteRepository routeRepository;
	
	@Autowired
	private ImageRepository imgRepository;
  
    @RequestMapping(path ="/metrics",method = RequestMethod.POST)
    public String greeting(@RequestBody Metric metric) {
    	metricRepository.save(metric);
    	return "hey";
    }
    
    
    @RequestMapping(path ="/metrics")
    public List<Metric> getMetrics() {
    	return metricRepository.findAll();
    	

    }
    
    @RequestMapping(path ="/routes", produces = { "application/xml", "text/xml" })
    public @ResponseBody RouteList getRoutes() {
    	RouteList list =  new RouteList();
    	list.setRoutes(routeRepository.findAll());
    	return list;
    	

    }
    
    @RequestMapping(value = "/image", method = RequestMethod.GET, produces = "image/png")
    public @ResponseBody byte[] getFile(@RequestParam(value="id") String id)  {
    	List<Image> imgs = imgRepository.findAll();
    	Iterator<Image> it = imgs.iterator();
    	String imgString = "";
    	while(it.hasNext()){
    		Image im = it.next();
    		if(im.get_id().equals(id)){
    			imgString = im.getEncodedImage();
    		}
    		
    	}
    	//String test = "56f99b80e4b05e6b92be8df9";
    	//String imgString = imgRepository.findOne(id).getEncodedImage();
    	//RouteList list =  new RouteList();
		//list.setRoutes(routeRepository.findAll());
		//int size = list.getRoutes().size();
		//int stepSize = list.getRoutes().get(size-1).getSteps().size();
		//String imgString = list.getRoutes().get(17).getSteps().get(0).getQuestions().get(0).getImage();
		// Retrieve image from the classpath.
		byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(imgString);
		return decoded;
    }
}

