package hw.treasureHunt.repository;


import org.springframework.data.mongodb.repository.MongoRepository;

import hw.treasureHunt.service.pojo.Metric;

public interface StatRepository extends MongoRepository<Metric, String> {



}

