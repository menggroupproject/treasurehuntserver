package hw.treasureHunt.repository;

import org.springframework.data.mongodb.repository.MongoRepository;


import hw.treasureHunt.service.pojo.Route;

public interface RouteRepository  extends MongoRepository<Route, String> {



}

