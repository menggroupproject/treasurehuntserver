package hw.treasureHunt.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import hw.treasureHunt.service.pojo.Image;


public interface ImageRepository extends MongoRepository<Image, String> {
	



}

