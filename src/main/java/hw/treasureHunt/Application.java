package hw.treasureHunt;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

import hw.treasureHunt.service.DataRestApi;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class Application  {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
   
}

//public class Application extends SpringBootServletInitializer {
//
//    public static void main(String[] args) {
//        SpringApplication.run(Application.class, args);
//    }
//    
//    @Override
//    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//        return application.sources(applicationClass);
//    }
//
//    private static Class<Application> applicationClass = Application.class;
//}
